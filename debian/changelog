lr (1.5.1-3) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: Let dh_auto_build pass cross tools to make
    Closes: #951558

  [ nicoo ]
  * Setup Salsa CI
  * d/changelog: Update nicoo's name and email address
  * Use upstream's canonical repo at vuxu.org in d/{copyright, watch}

 -- nicoo <nicoo@debian.org>  Mon, 02 Nov 2020 12:11:44 +0100

lr (1.5.1-2) unstable; urgency=medium

  * debian/rules
    + Honor build flags (incl. hardening)
    + Install upstream's changelog
    + Fail the build on dh_missing

  * debian/gbp.conf
    + Move the packaging branch to debian/sid, use DEP14
    + Fix pristine-tar usage
      gbp was producing a new, xz-compressed, upstream tarball
      even when a differently-compressed one was already available.

  * Update authorship information
  * debian/control: Update Homepage

 -- nicoo <nicoo@debian.org>  Sat, 15 Feb 2020 23:12:10 +0100

lr (1.5.1-1) unstable; urgency=medium

  * New upstream release (2020-01-18)
    + Feature: add -W to sort results by name and print during traversal.
    + Bug fix: Make -B keep traversing even if file is not printed.
    + Small speed tweaks.

  * debian/watch:
    + Switch to git mode
    + Check upstream's signature

  * Declare compliance with policy v4.5.0.
    No change required

 -- nicoo <nicoo@debian.org>  Thu, 30 Jan 2020 19:40:43 +0100

lr (1.4.1-1) unstable; urgency=medium

  * New upstream release (2019-02-08)

  * Make gbp commit (compressed) upstream tarballs.
    This avoids cases where the orig tarball is not generated reproducibly.
  * debian/control
    + Update maintainer's email address
    + Declare compliance with policy v4.3.0.
      No change required
    + Switch to debhelper 12.
      The dh compatibility level is now set by a build-depends.

 -- nicoo <nicoo@debian.org>  Sat, 09 Feb 2019 15:52:47 +0100

lr (1.4-1) unstable; urgency=medium

  * New upstream version (2018-04-17)

  * debian/control
    - Move the packaging repository to salsa.d.o
    - Declare compliance with policy version 4.1.4.
      No change required.
  * debian/rules: Drop redundant dh argument

 -- nicoo <nicoo@debian.org>  Mon, 30 Apr 2018 14:26:55 +0200

lr (1.2-1) unstable; urgency=medium

  * New upstream version (2017-11-17)
    * Performance improvements
    * `-U` now supports default width
    * New features
      * -B option for breadth-first search
      * New ternary `?:` operator
      * New `skip` action

 -- nicoo <nicoo@debian.org>  Thu, 23 Nov 2017 13:16:22 +0100

lr (1.1-2) unstable; urgency=medium

  * Fix build on hurd (Closes: #880852)

 -- nicoo <nicoo@debian.org>  Sat, 11 Nov 2017 23:44:19 +0100

lr (1.1-1) unstable; urgency=medium

  * Initial packaging (Closes: #861792)

 -- nicoo <nicoo@debian.org>  Mon, 30 Oct 2017 18:29:10 +0100

